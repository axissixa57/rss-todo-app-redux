import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TodoItem from './TodoItem';

class TodoList extends Component {
  // вызывается после рендеринга компонента
   componentDidMount() {
    // {type: "RECEIVE_TODOS", todos: Array(10)}
    this.props.getTodos();
  }

  render() {
    // 1. {todos: Array(0)...} 2. {todos: Array(10)...}
    const { todos, onTodoClick, deleteTodo } = this.props;
    
    return todos.map(todo => (
      <TodoItem
        key={todo.id}
        todo={todo}
        onTodoClick={onTodoClick}
        delTodo={deleteTodo}
      />
    ));
  }
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
  onTodoClick: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  getTodos: PropTypes.func.isRequired,
};

export default TodoList;
